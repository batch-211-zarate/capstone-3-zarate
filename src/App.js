import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/Navbar/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register/Register';
import Login from './pages/Login/Login';
import UserDetails from './pages/User/UserDetails';
import Cart from './pages/Cart/Cart';
import Dashboard from './components/Dashboard/Dashboard';
import Product from './pages/Product/Product';
import ProductView from './components/Product/ProductView';
import UpdateProduct from './components/Dashboard/UpdateProduct';
import UsersStatus from './components/Dashboard/UsersStatus';
import OrderHistory from './pages/OrderHistory/OrderHistory';
import Delivery from './pages/Delivery/Delivery';
// import PaymentOption from './pages/PaymentOption';
import Logout from './pages/Logout';
import Error from './pages/Error/Error';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }
  
  useEffect(() => {
    
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data);

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Product/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/deliveryShipping" element={<Delivery/>}/>
            {/* <Route path="/payment-option" element={<PaymentOption/>}/> */}
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/details" element={<UserDetails/>}/>
            <Route path="/admin" element={<Dashboard/>}/>
            <Route path="/updateProduct/:productId" element={<UpdateProduct/>}/>
            <Route path="/users" element={<UsersStatus/>}/>
            <Route path="/orderHistory" element={<OrderHistory/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<Error/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
