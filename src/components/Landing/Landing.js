
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LandingPage from '../../images/landing.jpg'
import './Landing.css';


export default function Landing() {
    return (
        <>
            <div className="landing">
                <div className="landing-container">
                    <div className="landing-image">
                        <img src={LandingPage} alt="landing-img" />
                        <span>Photo by <a href="https://unsplash.com/@_ferh97?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Fernando Hernandez</a> on <a href="https://unsplash.com/s/photos/monitor?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
                    </div>
                    <div className="landing-content">
                        <h1>Gadget Collections</h1>
                        <p>Just For You</p>
                        <Link to="/products"><Button>Shop Now</Button></Link>
                    </div>
                </div>
            </div>        
        </>
    );
}

  
