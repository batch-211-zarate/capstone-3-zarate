import { useState, useEffect, useContext } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import './Product.css';


export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate()

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const [isActive, setIsActive] = useState(false);

	const priceFormat = price.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});


	const addToBag = (productId, quantity) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/cart/add`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			if(data) {
				Swal.fire({
					text: data.message
				})
					navigate('/cart');
			} 
		})
	}

	const buyNow = (productId, quantity) => {
		console.log(productId);
		console.log(quantity);

		fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			if(data !== true) {
				Swal.fire({
					title: 'Purchased Confirmed!',
					text: data.message,
					icon: 'success'
				})
					navigate('/orderHistory');
			} else {
				Swal.fire({
					title: 'Error!',
					text: 'Something went wrong!',
					icon: 'error'
				})
			}
		})
	}

	useEffect(() => {
		if(quantity > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [quantity])

	useEffect(() => {

		// console.log(productId);
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])


	return (
		(user.id !== null)
		?
		<>
			<div className="product-container">
				<div className="image-container">
					<img src="https://via.placeholder.com/150" alt="product" />	
				</div>

				<div className="content-container">
					<div className="product-info">
						<h1>{name}</h1>
						<p className="description">{description}</p>
						<p className="price">{priceFormat}</p>
					</div>

					<div className="product-qty">
						<Form.Group controlId="productDescription">
							<Form.Label>Quantity</Form.Label>
							<Form.Control 
							type="number" 
							placeholder="Quantity" 
							value = {quantity}
							onChange = {e => setQuantity(e.target.value)}
							required
							/>
						</Form.Group>
					</div>

					<div className="cart-btn">
						<>
							{
								(isActive)
								? 
								<>
									<Button onClick={() => addToBag(productId, quantity)}>Add to Bag</Button>
									<Button onClick={() => buyNow(productId, quantity)}>Buy Now</Button>
								</>
								:
								<>
									<Button onClick={() => addToBag(productId, quantity)} disabled>Add to Bag</Button>
									<Button onClick={() => buyNow(productId, quantity)} disabled>Buy Now</Button>
								</>
							}
						</>
					</div>
				</div>
			</div>	
		</>
		:
		<Navigate to="/login" />
	);
}