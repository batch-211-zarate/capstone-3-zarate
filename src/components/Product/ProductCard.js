
import { Link } from 'react-router-dom';
import './Product.css';

export default function ProductCard({productProp}) {

	let { name, description, price, _id } = productProp;

	const priceFormat = price.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});

	return (

		<>	
			<div className="product-card">
				<Link className="card" to={`/products/${_id}`}>
					<div className="card-content">
						<div className="card-info">
							<h1 className="card-name">{name}</h1>
							<p className="card-description">{description}</p>
							<p className="card-price">{priceFormat}</p>
						</div>
					</div>
				</Link>
			</div>
		</>
	);
} 