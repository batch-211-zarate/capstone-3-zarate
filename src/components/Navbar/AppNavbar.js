import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../../UserContext'
import ShopLogo from '../../images/logo.svg';
import UserIcon from '../../images/user.svg';
import UserIconSolid from '../../images/user-solid.svg';
import CartIcon from '../../images/cart.svg';
import './AppNavbar.css';

export default function AppNavbar() {

    const { user } = useContext(UserContext);
    // console.log(user);

    return (
        <>
            <div className="header">
                <div className="header-left">
                    <Link to="/"><img  src={ShopLogo} alt="shop-logo" /></Link>
                </div>
                    
                <div className="header-middle">
                    <div className="header-option">
                        <span className="header-home">
                            <Link to="/">Home</Link>
                        </span>
                    </div>

                    <div className="header-option">
                        <span className="header-product">
                            <Link to="/products">Product</Link>
                        </span>
                    </div>

                    <div className="header-option">
                        <div className="dropdown">
                            <button className="drop-btn">Support
                            </button>
                            <div className="dropdown-content">
                                <Link to="/deliveryShipping">Delivery & Shipping</Link>
                                <Link to="/payment-option">Payment Option</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-right">
                    <div className="header-cart">
                        <Link to="/cart"><img src={CartIcon} alt="cart-icon" /></Link>
                    </div>
                    <div className="header-login">
                            {(user.id !== null)

                                ? 
                                <> 
                                    <div className="login-dropdown">
                                        <img src={UserIconSolid} alt="user-img" />Profile
                                        <div className="login-dropdown-content">
                                            <Link to="/details">Profile</Link>
                                            <Link to="/orderHistory">Order History</Link>
                                        </div>
                                    </div>
                                </>
                                : 
                                    <Link to="/login"><img src={UserIcon} alt="user-img" /></Link>
                            }
                    </div>
                    <div className="header-register">
                        {(user.id !== null)
                            
                            ? <Link to="/logout"><button>Logout</button></Link>
                            : <Link to="/register"><button>Get Started</button></Link>
                        } 
                    </div>
                    
                </div>
            </div>
            {/* <div className="header__search">
                <input className="search__input" type="text" placeholder="Search" />
            </div> */}
        </>
    );
}