import { useContext, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../../UserContext";
import './UserStatus.css';


export default function UsersStatus() {
    
    const {user} = useContext(UserContext);

    const [allUsers, setAllUsers] = useState([]);

    const fetchData = () => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setAllUsers(data.map(user => {
                return (
                    <tr key={user._id}>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.email}</td>
                        <td>{user.isAdmin ? "Admin" : "User"}</td>
                        <td>
                            {
                                (user.isAdmin)
                                ?
                                <Button variant="danger" size="sm" onClick ={() => setToUser(user._id)}>Demote</Button>
                                :
                                <Button variant="success" size="sm" onClick ={() => setToAdmin(user._id)}>Promote</Button>
                            }
                        </td>
                    </tr>
                )
            }))
        })
    }

    const setToAdmin = (userId) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isAdmin: true
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            if(data !== true) {
                alert(`Successfully promoted ${data.firstName} ${data.lastName} to admin!`);
            } 
            fetchData();
        })
    }

    const setToUser = (userId) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/admin/details/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isAdmin: false
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            if(data !== false) {
                alert(`Successfully demoted ${data.firstName} ${data.lastName} to user!`);
            }    
            fetchData();
        })

    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        (user.isAdmin)
        ?
        <>
            <div className="details-container">
                <h1>Users Details</h1>
                <Table className="styled-table" hover>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allUsers}
                    </tbody>
                </Table>
            </div>
        </>
        :
        <Navigate to="/" />
    )
}