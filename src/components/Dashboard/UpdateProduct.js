import { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Link, Navigate, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
import './UpdateProduct.css';


export default function UpdateProduct() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    const {productId} = useParams();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

    function updateProduct(e) {
        
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {

            // console.log(data);

            if(data) {
                Swal.fire({
	    		    title: "Product successfully Updated",
	    		    icon: "success",
	    		    text: `${name} is now updated`
	    		});

                navigate('/admin');

            } else {
                Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
            }
        })

        setName('');
        setDescription('');
        setPrice(0);
    }

    useEffect(() => {

        if(name !== "" && description !== "" && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

    useEffect(() => {

        console.log(productId);


        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {

            // console.log(data);
            
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [productId]);


    return (
        (user.isAdmin)
        ?
            <>
                <div className="update-product">
                    <h1>Update Product</h1>
                    <div className="update-product-container">
                        <div className="product-info">
                            <Form onSubmit={(e) => updateProduct(e)}>
                                <Form.Group controlId="productName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="Product Name" 
                                    value = {name}
                                    onChange = {e => setName(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group controlId="productDescription">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control 
                                    type="number" 
                                    placeholder="Price" 
                                    value = {price}
                                    onChange = {e => setPrice(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group controlId="productPrice">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control 
                                    as="textarea"
                                    rows={4}
                                    placeholder="Description" 
                                    value = {description}
                                    onChange = {e => setDescription(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                 { 
                                    (isActive) 
                                    ?
                                    <Button className="update-btn" variant="primary" type="submit">Update</Button>
                                    :
                                    <Button className="update-btn" variant="primary" type="submit" disabled>Update</Button>
                                }   
                                    <Link to="/admin"><Button className="cancel-btn" variant="primary" type="submit">Cancel</Button></Link>

                                    {/* <Button className="cancel-btn" variant="success" type="submit" onClick={handleClose}>
                                        Cancel
                                    </Button> */}
                            </Form>
                        </div>
                    </div>
                </div>
            </>
        :
        <Navigate to="/" />
    )
}