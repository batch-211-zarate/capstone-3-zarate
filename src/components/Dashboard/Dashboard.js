import { useContext, useState, useEffect } from "react";
import { Button, Form, Modal, Table } from "react-bootstrap";
import { Link, Navigate  } from "react-router-dom";
import UserContext from "../../UserContext";
import Swal from "sweetalert2";
import PlusIcon from "../../images/plus-solid.svg";
import ListIcon from "../../images/list-solid.svg";
import UpdateIcon from "../../images/update.svg";
import './Dashboard.css';


export default function Dashboard() {

    const {user} = useContext(UserContext);
    
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [allProducts, setAllProducts] = useState([]);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const fetchData = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            setAllProducts(data.map(product => {
                return (
                    <tr key={product._id}>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            
                            {
                                (product.isActive)
                                ?
                                <Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
                                :
                                <>
                                    <Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Activate</Button>

                                    <Link to={`/updateProduct/${product._id}`}><img className="update-icon" src={UpdateIcon} alt="update-img" /></Link> 
                                </>
                            }
                        </td>
                    </tr>
                )
            }))
        })
        
    }

    const archive = (productId, productName) => {
        // console.log(productId);
        // console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`,{
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data =>{
            // console.log(data);

            if(data){
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `${productName} is now inactive.`
                })
                fetchData();
            }
        })
    }

    const unarchive = (productId, productName) => {
		// console.log(productId);
		// console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/activate/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Activated Successful!",
					icon: "success",
					text: `${productName} is now active.`
				})
                fetchData();
			}
		})
	}
    
    function addProduct(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data) {
                Swal.fire({
                    title: "Product successfully Added",
                    icon: "success",
                    text: `${name} is now added`
                    
                });
                
                fetchData();
            } 
        })

        setName('');
        setDescription('');
        setPrice(0);

    }
    useEffect(() => {
        fetchData();
    }, [])


    useEffect(() => {

        if(name !== "" && description !== "" && price > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

    return (
        (user.isAdmin)
        ?
        <>
            <div className="dashboard">
                <div className="dashboard-container">
                    <h1>Dashboard</h1>
                    <div className="product-order-btn">
                        <div className="add-btn">
                            <>
                                <Button onClick={handleShow}><img className="plus-icon" src={PlusIcon} alt="plus-icon" /><span>Add Product</span></Button>

                                <Modal show={show} onHide={handleClose} aria-labelledby="contained-modal-title-vcenter" centered>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Add Product</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                         <>
                                            <div className="add-product">
                                                <div className="add-product-container">
                                                    <div className="product-info">
                                                        <Form onSubmit={(e) => addProduct(e)}>
                                                            <Form.Group controlId="productName">
                                                                <Form.Label></Form.Label>
                                                                <Form.Control 
                                                                type="text" 
                                                                placeholder="Product Name" 
                                                                value = {name}
                                                                onChange = {e => setName(e.target.value)}
                                                                required
                                                                />
                                                            </Form.Group>

                                                            <Form.Group controlId="productDescription">
                                                                <Form.Label></Form.Label>
                                                                <Form.Control 
                                                                type="number" 
                                                                placeholder="Price" 
                                                                value = {price}
                                                                onChange = {e => setPrice(e.target.value)}
                                                                required
                                                                />
                                                            </Form.Group>

                                                            <Form.Group controlId="productPrice">
                                                                <Form.Label></Form.Label>
                                                                <Form.Control 
                                                                as="textarea"
                                                                rows={4}
                                                                placeholder="Description" 
                                                                value = {description}
                                                                onChange = {e => setDescription(e.target.value)}
                                                                required
                                                                />
                                                            </Form.Group>

                                                            { isActive 
                                                                ?
                                                                <Button className="create-btn" variant="primary" type="submit">
                                                                    Create
                                                                </Button>
                                                                :
                                                                <Button className="create-btn" variant="primary" type="submit" disabled>
                                                                    Create
                                                                </Button> 
                                                            }  
                            
                                                        </Form>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                        
                                    </Modal.Body>
                                </Modal>   
                            </>
                        </div>

                        <div className="users-btn">
                            <Link to="/users"><Button><img className="list-icon" src={ListIcon} alt="list-icon" /><span>Users</span></Button></Link>
                        </div>

                        {/* <div className="show-btn">
                            <Link to="/usersOrder"><Button><img className="list-icon" src={ListIcon} alt="list-icon" /><span>Users Orders</span></Button></Link>
                        </div> */}
                    </div>
                    <Table className="styled-table" hover>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allProducts}
                        </tbody>
                    </Table>
                </div>
            </div>
        </>
        :
        <Navigate to="/" />
    );
}