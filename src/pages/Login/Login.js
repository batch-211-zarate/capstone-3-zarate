import { Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import LoginImage from '../../images/login-image.jpg';
import './Login.css';


export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState('');

    const {user, setUser} = useContext(UserContext);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method:'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to Gshop!"
                });
            } else {
                Swal.fire({
                    title: "Login Unsuccessful!",
                    icon: "error",
                    text: data.message
                });
            }
        })
        
        const retrieveUserDetails = (token) => {

            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {

                setUser({
                    id:data._id,
                    isAdmin:data.isAdmin
                });
            })
        }

        setEmail("");
        setPassword("");
    }

    useEffect(() => {

    if ( email !== "" && password !== "" ) {
        setIsActive(true)
    } else {
        setIsActive(false)
    }

    }, [email, password])

    return (
        (user.id !== null)
        ?
        <Navigate to="/" />
        :
        <>
            <div className="login">
                <div className="login-container">
                    <div className="login-info">
                        <Form onSubmit={(e) => authenticate(e)}>
                            <h1 className="login-title">Login</h1>

                            <Form.Group controlId="userEmail">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="email" 
                                placeholder="Email" 
                                value = {email}
                                onChange = {e => setEmail(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group controlId="password1">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value = {password}
                                onChange = {e => setPassword(e.target.value)}
                                required
                                />
                            </Form.Group>

                            { isActive ?
                                <Button className="login-btn"  type="submit">
                                    Login
                                </Button>
                                :
                                <Button className="login-btn" type="submit">
                                    Login
                                </Button>
                            }

                            <p className="login-terms">Don't have an account? <Link to="/register">Register Now.</Link></p>

                        </Form>
                    </div>
                    <div className="login-image">
                        <img src={LoginImage} alt="login-img"/>
                        <span>Photo by <a href="https://unsplash.com/@johnny_2d?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ivan Didenko</a> on <a href="https://unsplash.com/s/photos/gadget?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
                        <p>Welcome to Gshop</p>
                    </div>
                </div>
            </div>
        </>
    );
}
