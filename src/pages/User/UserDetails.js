import { useContext, useEffect, useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../../UserContext";
import './UserDetails.css';


export default function UserDetails() {

    const {user} = useContext(UserContext);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');


    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            setFirstName(data.firstName);
            setLastName(data.lastName);
            setMobileNo(data.mobileNo);
            setEmail(data.email);
            setPassword(data.password);
        })
    }, []);


    return (
        (user.id !==null)
        ?
            <>
                <div className="user-container">
                    <div className="user-info">
                        <Form>
                            <Row>
                                <h1 className="user-title">Profile</h1>
                                <Form.Group as={Col} controlId="firstName">
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="First Name" 
                                    value = {firstName}
                                    onChange = {e => setFirstName(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group as={Col} controlId="lastName">
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="Last Name" 
                                    value = {lastName}
                                    onChange = {e => setLastName(e.target.value)}
                                    required
                                    />
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col}  controlId="mobileNo">
                                <Form.Label>Contact</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Mobile No." 
                                value = {mobileNo}
                                onChange = {e => setMobileNo(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control 
                                type="email" 
                                placeholder="Email" 
                                value = {email}
                                onChange = {e => setEmail(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value = {password}
                                onChange = {e => setPassword(e.target.value)}
                                required
                                />
                            </Form.Group>
                        </Form>
                    </div>
                </div>
            </>
        :
        <Navigate to="/login" />
        
    )
}