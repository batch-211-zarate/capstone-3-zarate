
import { useEffect, useState } from 'react';

import './OrderHistory.css';

export default function OrderHistory() {

    const [orders, setOrders] = useState([]);

   const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            // console.log(typeof(data));
            setOrders(data.map(item => {
                for (let i = 0; i < item.products.length; i++) {
                        // console.log(item.products[i]);

                        // const priceSubtotal = item.products[i].subtotal.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});

                    const price = item.products[i].price.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});


                    const priceTotal = item.totalAmount.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});

                    return (
                        <div className="cart-item" key={item._id}>
                            <div className="cart-item-image">
                                <img src="https://via.placeholder.com/150" alt="product" />	
                            </div>
                            <div className="cart-item-details">
                                <div className="cart-item-name">
                                    <p>{item.products[i].name}</p>
                                </div>
                                <div className="cart-item-description">
                                    <p>{item.products[i].description}</p>
                                </div>
                                <div className="cart-item-price">
                                    <p>{price}</p>
                                </div>
                                <div className="cart-item-quantity">
                                    <p>Quantity: {item.products[i].quantity}</p>
                                </div>
                                <div className="cart-item-total">
                                    <p>Total: {priceTotal}</p>
                                </div>
                            </div>
                        </div>
                    )
                }
                
            }))
        })
    }

     useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
            <div className="order-history">
                <div className="order-history-heading">
                    <h1>Order History</h1>
                </div>
                <div className="orders">
                    {orders}
                </div>
            </div>
        </>
    )


}
