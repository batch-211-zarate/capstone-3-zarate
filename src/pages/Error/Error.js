import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import './Error.css';


export default function Error() {
  return (
    <>
      <div className="error">
          <div className="error-info">
              <h1>Page Not Found</h1>
              <p>The page you are looking for cannot be found</p>
              <Link to="/products"><Button className="home-btn">Continue Shopping</Button></Link>
          </div>
      </div>
    </>
  )
}