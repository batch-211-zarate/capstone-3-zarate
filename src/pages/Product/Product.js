import { useEffect, useState, useContext } from 'react';
import { Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import ProductCard from '../../components/Product/ProductCard';
import UserContext from '../../UserContext';
import './Product.css';


export default function Product() {

    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext);
	// console.log(user);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            const productArr = (data.map(product => {
                return (
                    <ProductCard key = {product._id} productProp = {product}/>
                )
            }))

            setProducts(productArr);
        })
    }, [])


    return (

        (user.isAdmin)
        ?
        <Navigate to="/admin"/>
        :
        <>  
            <div className="product">
                <Row>
                    {products}
                </Row>
            </div>
        </>
    );
}