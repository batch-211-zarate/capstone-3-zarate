import { Button, Col, Row, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import RegisterImage from '../../images/register-image.jpg';
import './Register.css';


export default function Register() {

    const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState('');

    function registerUser(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/signup`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
                email: email,
                password: password1,
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data !== true) {
                setFirstName("");
                setLastName("");
                setMobileNo("");
                setEmail("");
                setPassword1("");
                setPassword2("");

                Swal.fire({
                    title: 'Success!',
                    text: data.message,
                    icon: 'success'
                });

                navigate('/login');

            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    text: "Please try again!",
                    icon: 'error'
                });
            }
        })
    }


    useEffect(() => {

        if ((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [firstName, lastName, mobileNo, email, password1, password2])

    return (
        (user.id !== null)
        ?
        <Navigate to="/" />
        :
        <>
            <div className="register">
                <div className="register-container">
                    <div className="register-image">
                        <img src={RegisterImage} alt="desktop"/>
                        <span>Photo by <a href="https://unsplash.com/@screenpost?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">SCREEN POST</a> on <a href="https://unsplash.com/s/photos/gadget?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
                        <p>Welcome to Gshop</p>
                    </div>
                    <div className="register-info">
                        <Form onSubmit={(e) => registerUser(e)}>
                            <Row>
                                <h1 className="register-title">Register</h1>
                                <Form.Group as={Col} controlId="firstName">
                                    <Form.Label></Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="First Name" 
                                    value = {firstName}
                                    onChange = {e => setFirstName(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group as={Col} controlId="lastName">
                                    <Form.Label></Form.Label>
                                    <Form.Control 
                                    type="text" 
                                    placeholder="Last Name" 
                                    value = {lastName}
                                    onChange = {e => setLastName(e.target.value)}
                                    required
                                    />
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col}  controlId="mobileNo">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Mobile No." 
                                value = {mobileNo}
                                onChange = {e => setMobileNo(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="email" 
                                placeholder="Email" 
                                value = {email}
                                onChange = {e => setEmail(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value = {password1}
                                onChange = {e => setPassword1(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password2">
                                <Form.Label></Form.Label>
                                <Form.Control 
                                type="password" 
                                placeholder="Confirm Password" 
                                value = {password2}
                                onChange = {e => setPassword2(e.target.value)}
                                required
                                />
                            </Form.Group>

                            { isActive ?
                                <Button className="register-btn"  type="submit">
                                    Register Now
                                </Button>
                                :
                                <Button className="register-btn" type="submit" disabled>
                                    Register Now
                                </Button>
                            }

                            <p className="register-terms">Already have an account? <Link to="/login">Login now.</Link></p>

                        </Form>
                    </div>
                </div>
            </div>
        </>
    );
}



