
import { useContext, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../../UserContext';
import PlusIcon from '../../images/plus-solid.svg';
import ChevronIcon from '../../images/chevron-right-solid.svg';
import DeleteIcon from '../../images/trash-solid.svg';
import Swal from 'sweetalert2';
import './Cart.css';

export default function Cart() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate()

    const [cart, setCart] = useState([]);

  
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            // console.log(typeof(data));

            setCart(data.map((item) => {
                // console.log(item);

                for (let i = 0; i < item.products.length; i++) {

                    // console.log(item.products.length);
                    // console.log(item.products[i]);
    
                    const price = item.products[i].price.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});
    
                    const priceSubtotal = item.products[i].subtotal.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});
    
                    const priceTotal = item.totalAmount.toLocaleString(undefined, {style: 'currency', currency: 'PHP'});
    
                    return (
                        <div className="cart-item" key={item._id}>
                            <div className="cart-item-image">
                                <img src="https://via.placeholder.com/150" alt="product" />	
                            </div>
                            <div className="cart-item-details">
                                <div className="cart-item-name">
                                    <p>{item.products[i].name}</p>
                                </div>
                                <div className="cart-item-delete">
                                    <span onClick={() => deleteProduct(item.products[i].productId, item.products[i].quantity)}><img src={DeleteIcon} alt="delete-img" /></span>
                                </div>
                                <div className="cart-item-description">
                                    <p>{item.products[i].description}</p>
                                </div>
                                <div className="cart-item-price">
                                    <p>{price}</p>
                                </div>
                                <div className="cart-item-quantity">
                                    <Form.Group controlId="productDescription">
                                        <Form.Label>Quantity</Form.Label>
                                        <Form.Control 
                                        type="number" 
                                        placeholder="Price" 
                                        value = {item.products[i].quantity}
                                        onChange = {(e) => updateQuantity(item.products[i].productId, e.target.value)}
                                        required
                                        />
                                    </Form.Group>
                                </div>
                                <div className="cart-item-subtotal">
                                    <p>Subtotal: {priceSubtotal}</p>
                                </div>
                                <div className="cart-item-total">
                                    <p>Total: {priceTotal}</p>
                                </div>
                                <div className="checkout-btn">`
                                    <Button onClick={() => checkout(item.products[i].productId, item.products[i].quantity)}><span>Checkout</span><img className="chevron-icon" src={ChevronIcon} alt="plus-icon" /></Button>
                                </div>
                            </div>
                        </div>
                    )
                }
            
            }))
        })
    }


    const deleteProduct = (productId, quantity) => {
        console.log(productId);
        console.log(quantity);

        fetch(`${process.env.REACT_APP_API_URL}/orders/cart/delete`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                Authorization:`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    text: data.message,
                    icon: 'success',
                })
                fetchData();
            }

        })
    }

    const updateQuantity = (productId, quantity) => {
        console.log(productId);
        console.log(quantity);

        fetch(`${process.env.REACT_APP_API_URL}/orders/cart/update`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization:`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    text: "Quantity updated",
                    icon: 'success'
                })
                fetchData();
            }

        })
    }

    
    const checkout = (productId, quantity) => {
        console.log(productId);
        console.log(quantity);

        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization:`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    text: data.message,
                    icon: 'success'
                })
                navigate('/orderHistory');
            }
        })

    }
    
    useEffect(() => {
        fetchData();
    }, []) 

    
    return (
        (user.id !== null)
        ?
        <>  
            <div className="cart-container">`
                <h1>Cart</h1>
                <div className="btn-container">
                    <div className="add-btn">
                        <Link to="/products"><Button><img className="plus-icon" src={PlusIcon} alt="plus-icon" /><span>Add More</span></Button></Link>    
                    </div>
                </div>
                {cart}

            </div>
        </>
        :
        <Navigate to="/login" />
    )
}